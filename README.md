# FramesExtractor

Extract a frames from a video

## How to use

`$ ./framy -s [start_time] -t [duration_time] [name] [directory] [video.mp4]`

where: 
*	`[start_time]` is the start time to cut the input video (format: hh:mm:ss.ms)
*	`[duration_time]` is the duration of the output gif (format: hh:mm:ss.ms)
*	`[name]` is the output name of the gif
*	`[directory]` is the directory in which it will be saved the gif
*	`[video.mp4]` is the output video.

### Dependency

`ffmpeg`


### TO-DO

for gifmy white artifacts we first need to generate a palette. Try this.

###  Create an MPEG movie. This example creates one from a series of images, then shrinks it to 288 × 162 pixels.
```    
fmpeg -i %04d.png -s 288x162 test.mp4
```
### Generate a palette using either the lanczos or dither filter. It creates a small palette file in a png image format.
```    
    ffmpeg -i test.mp4 -vf scale=900:-1:flags=lanczos,palettegen palette.png
    ffmpeg -i test.mp4 -vf scale=900:-1:sws_dither=ed,palettegen palette2.png
```
### Convert the mpeg into an animated gif using this rather complicated command. 
```
ffmpeg -i test.mp4 -i palette2.png -filter_complex "fps=30,scale=250:-1:flags=lanczos[x];[x][1:v]paletteuse" test.gif 
```

please look here https://www.randombio.com/linuxsetup141.html
